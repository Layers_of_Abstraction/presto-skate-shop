﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PrestoSkateShop.Models;
using PrestoSkateShop.TableClasses;
using System.Data;
using System.Diagnostics;
using FastMember;
using System.ComponentModel;

namespace PrestoSkateShop
{
    public partial class ProductList : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {            
                if (IsPostBack)
                    return;
                
            //PROBLEM: To get products and categories list and display them on screen.
                //GET Product list 
                //GET Category list
                //CONVERT list to data table
                //COMBINE data tables into one data table
                //Bind Data table to grid view
                //END

                DataTable table = new DataTable();
                IEnumerable<Product> ProductData = Product.GetAllProducts();
                using (var reader = ObjectReader.Create(ProductData))
                {
                    table.Load(reader);
                }

                IEnumerable<Category> CategoryData = Category.GetAllCategorys();
                using (var reader = ObjectReader.Create(CategoryData))
                {
                    table.Load(reader);
                }


                gvList.DataSource = table;
                gvList.DataBind();
        }

        protected void gvList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Session[Strings.SessionVariables.ProductID] = gvList.SelectedRow.Cells[1].Text;
            //Response.Redirect(Strings.Pages.ProductDetails);    
        }


        private void convertCategoryListToDT()
        {
            IEnumerable<Category> data = Category.GetAllCategorys();
            DataTable table = new DataTable();
            using (var reader = ObjectReader.Create(data))
            {
                table.Load(reader);
                gvList.DataSource = table;
            }
        }

        
    }
}