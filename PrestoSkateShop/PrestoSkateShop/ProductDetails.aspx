﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductDetails.aspx.cs" Inherits="PrestoSkateShop.ProductDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div>        
        <asp:UpdatePanel ID="panel" runat="server">
        <ContentTemplate>    
        <table>
            <thead>
                <tr>
                    <td style="height: 3px"><table style="width:20%"></table></td>
                    <td style="height: 3px"><table style="width:20%"></table></td>
                </tr>
            </thead>
            <tr>
            <td><h1>PRODUCTS</h1></td>
            </tr>
            <tr>
            <td>ID:</td>
            <td><asp:Label ID ="lblID" runat="server" /></td>
            </tr>
            <tr>
            <td>Category Name:</td>
            <td><asp:DropDownList ID="ddlCategoryName" runat="server" /></td>
            </tr>
            <tr>
            <td>Product:</td>
            <td><asp:TextBox ID="txtProductName" runat="server" /></td>
            </tr>
            <tr>
            <td>Price:</td>
            <td><asp:TextBox ID="txtProductPrice" runat="server" /></td>
            </tr>
            <%--<tr>
            <td>Image:</td>
            <td><asp:TextBox ID="txtProductImage" runat="server" /></td>
            </tr>--%>
            <tr>
            <td>Colour:</td>
            <td><asp:TextBox ID="txtColour" runat="server" /></td>
            </tr>
            <tr>
            <td>Size:</td>
            <td><asp:TextBox ID="txtSize" runat="server" /></td>
            </tr>
            <tr>            
            <td>Description:</td>
            <td><asp:TextBox ID="txtDescription" runat="server" /></td>
            </tr>
        </Table>
    </div>
    
    <!--Table for buttons-->
    <div>
        <table>
            <tr>
                <td><asp:Button ID ="btnDelete" Text="Delete" runat="server"/></td>
                <td><asp:Button ID ="btnSave" Text="Save" runat="server" /></td>        
            </tr>
        </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    </div>
</asp:Content>
