﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductList.aspx.cs" Inherits="PrestoSkateShop.ProductList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="sidebar topspace30">
                    <hr />
                    <div class="wowwidget">
                        <h4>Categories</h4>
                        <ul class="categories">
                            <li><a href="#">Music & Films</a></li>
                            <li><a href="#">Home Decoration</a></li>
                            <li><a href="#">Food & Health</a></li>
                            <li><a href="#">Lifestyle Green</a></li>
                            <li><a href="#">Future Technology</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="page-header">
                <div class="container">
                    <div class="row">
                        <h1>Products</h1><asp:GridView ID="gvList"  runat="server" AutoGenerateColumns="False" OnSelectedIndexChanged="gvList_SelectedIndexChanged">
                                <Columns>       
                                   <asp:CommandField ShowSelectButton="True" />
                                    <asp:TemplateField HeaderText="Product ID">
                                        <ItemTemplate> <%# Eval("ID")%> 
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    
                                   <asp:TemplateField HeaderText="Category">
                                        <ItemTemplate> <%# Eval("CategoryName")%> 
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>                                    
                                    <asp:TemplateField HeaderText="Product Name">
                                        <ItemTemplate> <%# Eval("ProductName")%> 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Product Price">
                                        <ItemTemplate> <%# Eval("Price")%> 
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Product Colour">
                                        <ItemTemplate> <%# Eval("Colour")%> 
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Product Size">
                                        <ItemTemplate> <%# Eval("Size")%> 
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:ImageField HeaderText="Image" DataImageUrlField="Image" 
                                        ControlStyle-Height="100px" ControlStyle-Width="100px"> 
                                       <ControlStyle Height="100px" Width="100px"></ControlStyle>
                                    </asp:ImageField>

                                   <%--<asp:TemplateField HeaderText="Product Description">
                                        <ItemTemplate> <%# Eval("Description")%> 
                                        </ItemTemplate>
                                   </asp:TemplateField>  --%>
                                </Columns>
                            </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
