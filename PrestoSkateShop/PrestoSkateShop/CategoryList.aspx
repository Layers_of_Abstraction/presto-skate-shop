﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CategoryList.aspx.cs" Inherits="PrestoSkateShop.CategoryList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <div class="container">
            <div class="row">
                <h1>Categories</h1><asp:GridView ID="gvList"  runat="server" OnSelectedIndexChanged="gvList_SelectedIndexChanged" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1">
                <Columns>
                    <asp:CommandField ShowEditButton="True" />  
                    <asp:BoundField DataField="ID" HeaderText="ID" 
                        SortExpression="ID" ReadOnly="true"/>
                    <asp:BoundField DataField="CategoryName" HeaderText="Category" SortExpression="CategoryName" />                  
                </Columns>
                </asp:GridView>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetAllCategorys" TypeName="PrestoSkateShop.TableClasses.Category">
                </asp:ObjectDataSource>                
            </div>
        </div>
    </div>
</asp:Content>
