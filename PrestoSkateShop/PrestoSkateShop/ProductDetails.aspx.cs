﻿using PrestoSkateShop.Models;
using PrestoSkateShop.TableClasses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PrestoSkateShop
{
    public partial class ProductDetails : System.Web.UI.Page
    {
        static Product _objProduct = new Product();
        static Category _objCategory = new Category();

        public ProductDetails() {}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) 
                return;
            try
            {
                long recordID = long.Parse(Session[Strings.SessionVariables.ProductID].ToString());
                _objProduct = Product.getRecordByID(recordID);
                lblID.Text = _objProduct.ID.ToString();
                ddlCategoryName.Text = _objProduct.CategoryID.ToString();
                /*ddlCategoryID.text = _objProduct.CategoryID.ToString();  <-- MAKE SURE WE IMPLEMENTED THIS!!!
                                                                               ALSO SEE IF WE CAN BIND THE DATA                

                txtDesc.Text = _objProduct.Description;
                txtPrice.Text = _objProduct.Price.ToString();*/

            }
            catch (Exception ex)
            {
                _objProduct = new Product(); // drop a fresh product in memory - if this catch is triggered, 
                                             // it's probably because the operator in memory doesn't match 
                                             // the operator being edited/created
                lblID.Text = "0";
                Debug.WriteLine("ERROR: " + ex.Message);
            }
        }
    }
}