﻿using PrestoSkateShop.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrestoSkateShop.TableClasses
{
    public class Category
    {
        
        #region Object & Query Declarations

        private DataSet _ds = new DataSet();
        private DataRow _dr = null;
        int _intMaxRows;
        int _intCounter = 0;
        DatabaseConnection _objConnect = new DatabaseConnection(Models.Strings.CONNECTION_STRING);
       
        //Query declarations
        private const string QUERY_SELECT = "SELECT * FROM tblCategories ORDER BY CategoryName ASC ";
        private const string QUERY_SELECT_ONE = "SELECT * FROM tblCategories WHERE CategoryID =({0})";
        private const string QUERY_UPDATE = "UPDATE tblCategories SET CategoryName=('{1}') WHERE CategoryID =({0})";
        private const string QUERY_INSERT = "INSERT INTO tblCategories (CategoryName) VALUES ('{0}')";
        private const string QUERY_DELETE = "DELETE FROM tblCategories WHERE CategoryID = ({0})";

        
        #endregion Object & Query Declarations

        #region Properties

        //ID is PK
        public long ID { get; set; }
        public string CategoryName { get; set; }        
        

        #endregion        

        #region Constructors

        public Category() { }       

        #endregion

        #region Initalizers

        /// <summary>
        /// Displays a whole grid of data
        /// </summary>
        /// <param name="ds">The datagrid that is being filled with data and
        /// passed back to the presentation layer once it's filled</param>
        /// <returns>data set after it's filled</returns>
        public DataSet loadData(DataSet ds)
        {
            _objConnect.ConnString = Models.Strings.CONNECTION_STRING;
            //SELECT all from Categorys
            _objConnect.QueryString = QUERY_SELECT;
            //GET table from data set in connection class.
            ds = _objConnect.getDataset;
            //PUT all data into class data set.
            _intMaxRows = ds.Tables[0].Rows.Count;
            //Count out or the data rows in data set.
            _dr = ds.Tables[0].Rows[_intCounter];
            return ds;            
        }
        
        /// <summary>
        /// Loads currently selected record 
        /// </summary>
        /// <param name="id">The Primary key to identify the current record</param>
        /// <returns>The current record</returns>
        public static Category getRecordByID(long id)
        {
            string query = string.Format(QUERY_SELECT_ONE, id);
            SqlConnection dbConn = new SqlConnection(Models.Strings.CONNECTION_STRING);
            SqlCommand cmd = new SqlCommand(query, dbConn);
            Category result = new Category();

            dbConn.Open();

            using (SqlDataReader  reader = cmd.ExecuteReader())
            {
                reader.Read();
                result.ID = long.Parse(reader[0].ToString());
                result.CategoryName = reader[1].ToString();                
            }
            dbConn.Close();
            return result;
        }


        public static void UpdateCategory(int ID, string Name)
        {
            using (SqlConnection con = new SqlConnection(Models.Strings.CONNECTION_STRING))
            {
                string updateQuery = "Update tblCategories SET CategoryName = @CategoryName" +
                " WHERE CategoryID = @CategoryID";
                SqlCommand cmd = new SqlCommand(updateQuery, con);
                SqlParameter paramCategoryID = new SqlParameter("@CategoryID", ID);
                cmd.Parameters.Add(paramCategoryID);
                SqlParameter paramName = new SqlParameter("@CategoryName", Name);
                cmd.Parameters.Add(paramName);                
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Loads an entire list of Categorys.
        /// </summary>
        public static List<Category> GetAllCategorys()
        {
            List<Category> listCategorys = new List<Category>();
            using (SqlConnection conn = new SqlConnection(Models.Strings.CONNECTION_STRING))
            {
                SqlCommand cmd = new SqlCommand(QUERY_SELECT, conn);
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Category Category = new Category();
                    Category.ID = (long)rdr[0];
                    Category.CategoryName = rdr[1].ToString();
                    listCategorys.Add(Category);
                }
            }
            return listCategorys;
        }
        #endregion

        #region Methods & functions
        /// <summary>
        /// Binds the data to a user event based on wether the record
        /// is blank. If it's an existing record then the record
        /// get's updated. It's all determinded. 
        /// </summary>
        public void SaveData()
        {            
            if (ID == 0)
            {
                AddNewRecord();
            }

            else
            {
                UpdateRecord();
            }            
            _objConnect.updateDB(_ds);            
        }

        /// <summary>
        /// This method is bound to the SaveData() method and will add a record from the user event that calls 
        /// the SaveData method.  
        /// </summary>
        private void AddNewRecord()
        {
            string query = "";
            query = string.Format(QUERY_INSERT, this.ID, this.CategoryName);
            SqlConnection dbConn = new SqlConnection(Models.Strings.CONNECTION_STRING);
            SqlCommand dbCmd = new SqlCommand(query, dbConn);
            dbConn.Open();
            dbCmd.ExecuteNonQuery();             
            dbConn.Close();
        }

        /// <summary>
        /// This method is bound to the SaveData() method and will 
        /// update a record from the user event that calls 
        /// the SaveData method.  
        /// </summary>
        private void UpdateRecord()
        {
            string query = "";
            query = string.Format(QUERY_UPDATE, this.ID, this.CategoryName);
            SqlConnection dbConn = new SqlConnection(Models.Strings.CONNECTION_STRING);
            SqlCommand dbCmd = new SqlCommand(query, dbConn);
            dbConn.Open();
            dbCmd.ExecuteNonQuery();
            dbConn.Close();
        }

        /// <summary>
        /// This method will simply delete the current record selected by a user event. 
        /// It is independent of other methods and can be called all by it's self. 
        /// </summary>
        public void DeleteRecord()
        {
            string query = "";
            query = string.Format(QUERY_DELETE, this.ID);
            SqlConnection dbconn = new SqlConnection(Models.Strings.CONNECTION_STRING);
            SqlCommand cmd = new SqlCommand(query, dbconn);
            dbconn.Open();
            cmd.ExecuteNonQuery();
            dbconn.Close();
        }
        #endregion Methods & functions        
    }    
}
