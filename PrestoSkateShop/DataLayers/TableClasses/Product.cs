﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using PrestoSkateShop.Models;
using System.Threading.Tasks;

namespace PrestoSkateShop.TableClasses
{
    
    public class Product
    {
        #region Object & Query Declarations

        private DataSet _ds = new DataSet();
        private DataRow _dr = null;
        int _intMaxRows;
        int _intCounter = 0;
        DatabaseConnection _objConnect = new DatabaseConnection(Models.Strings.CONNECTION_STRING);

        //Query declarations
        private const string QUERY_SELECT = "SELECT * FROM tblProducts ORDER BY ProductName ASC ";
        private const string QUERY_SELECT_ONE = "SELECT * FROM tblProducts WHERE ProductID =({0})";
        private const string QUERY_UPDATE = "UPDATE tblProducts SET ProductName=('{1}'), ProductPrice =({2}), ProductImage = ('{3}')" +
                                                                     " ProductColour=('{4}'), ProductSize=('{5}'), ProductDescription =('{6}') WHERE ProductID =({0})";
        private const string QUERY_INSERT = "INSERT INTO tblProducts (ProductName, ProductPrice, ProductImage, ProductColour, ProductSize, ProductDescription) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')";
        private const string QUERY_DELETE = "DELETE FROM tblProducts WHERE ProductID = ({0})";

        
        #endregion Object & Query Declarations

        #region Properties

        //ID is PK
        public long ID { get; set; }
        public long CategoryID { get; set; }
        public string ProductName { get; set; }        
        public decimal Price { get; set; }
        public string Image { get; set; }
        public string Colour { get; set; }
        public string Size { get; set; }  
        public string Description { get; set; }

        #endregion        

        #region Constructors

        public Product() { }       

        #endregion

        #region Initalizers

        /// <summary>
        /// Displays a whole grid of data
        /// </summary>
        /// <param name="ds">The datagrid that is being filled with data and
        /// passed back to the presentation layer once it's filled</param>
        /// <returns>data set after it's filled</returns>
        public DataSet loadData(DataSet ds)
        {
            _objConnect.ConnString = Models.Strings.CONNECTION_STRING;
            //SELECT all from products
            _objConnect.QueryString = QUERY_SELECT;
            //GET table from data set in connection class.
            ds = _objConnect.getDataset;
            //PUT all data into class data set.
            _intMaxRows = ds.Tables[0].Rows.Count;
            //Count out or the data rows in data set.
            _dr = ds.Tables[0].Rows[_intCounter];
            return ds;            
        }
        
        /// <summary>
        /// Loads currently selected record 
        /// </summary>
        /// <param name="id">The Primary key to identify the current record</param>
        /// <returns>The current record</returns>
        public static Product getRecordByID(long id)
        {
            string query = string.Format(QUERY_SELECT_ONE, id);
            SqlConnection dbConn = new SqlConnection(Models.Strings.CONNECTION_STRING);
            SqlCommand cmd = new SqlCommand(query, dbConn);
            Product result = new Product();

            dbConn.Open();

            using (SqlDataReader  reader = cmd.ExecuteReader())
            {
                reader.Read();
                result.ID = long.Parse(reader[0].ToString());
                result.Description = reader[1].ToString();
                result.Price = decimal.Parse(reader[2].ToString());
                //result.Image = reader[3].ToString();  ADVISE COMMENTING OUT THIS
                //result.Image = byte.Parse(reader[3]); ELEMENT MAY RESULT IN ERROR
                result.Colour = reader[4].ToString();
                result.Size = reader[5].ToString();
                result.Description = reader[6].ToString();
                //CREATE IMAGE LOADING METHOD
            }
            dbConn.Close();
            return result;
        }

        /// <summary>
        /// Loads an entire list of products.
        /// </summary>
        public static List<Product> GetAllProducts()
        {
            List<Product> listProducts = new List<Product>();
            using (SqlConnection conn = new SqlConnection(Models.Strings.CONNECTION_STRING))
            {
                SqlCommand cmd = new SqlCommand(QUERY_SELECT, conn);
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Product product = new Product();
                    product.ID = (long)rdr[0];
                    product.CategoryID = (long)rdr[1];
                    product.ProductName = rdr[2].ToString();
                    product.Price = decimal.Parse(rdr[3].ToString());
                    product.Image = rdr[4].ToString();
                    product.Colour = rdr[5].ToString();
                    product.Size = rdr[6].ToString();
                    product.Description = rdr[7].ToString();
                    listProducts.Add(product);
                }
            }
            return listProducts;
        }
        #endregion

        #region Methods & functions
        /// <summary>
        /// Binds the data to a user event based on wether the record
        /// is blank. If it's an existing record then the record
        /// get's updated. It's all determinded. 
        /// </summary>
        public void SaveData()
        {            
            if (ID == 0)
            {
                AddNewRecord();
            }

            else
            {
                UpdateRecord();
            }            
            _objConnect.updateDB(_ds);            
        }

        /// <summary>
        /// This method is bound to the SaveData() method and will add a record from the user event that calls 
        /// the SaveData method.  
        /// </summary>
        private void AddNewRecord()
        {
            string query = "";
            query = string.Format(QUERY_INSERT, this.Description, this.Price);
            SqlConnection dbConn = new SqlConnection(Models.Strings.CONNECTION_STRING);
            SqlCommand dbCmd = new SqlCommand(query, dbConn);
            dbConn.Open();
            dbCmd.ExecuteNonQuery();             
            dbConn.Close();
        }

        /// <summary>
        /// This method is bound to the SaveData() method and will 
        /// update a record from the user event that calls 
        /// the SaveData method.  
        /// </summary>
        private void UpdateRecord()
        {
            string query = "";
            query = string.Format(QUERY_UPDATE, this.ID, this.Description, this.Price, this.Image, this.Colour, this.Size, this.Description);
            SqlConnection dbConn = new SqlConnection(Models.Strings.CONNECTION_STRING);
            SqlCommand dbCmd = new SqlCommand(query, dbConn);
            dbConn.Open();
            dbCmd.ExecuteNonQuery();
            dbConn.Close();
        }

        /// <summary>
        /// This method will simply delete the current record selected by a user event. 
        /// It is independent of other methods and can be called all by it's self. 
        /// </summary>
        public void DeleteRecord()
        {
            string query = "";
            query = string.Format(QUERY_DELETE, this.ID);
            SqlConnection dbconn = new SqlConnection(Models.Strings.CONNECTION_STRING);
            SqlCommand cmd = new SqlCommand(query, dbconn);
            dbconn.Open();
            cmd.ExecuteNonQuery();
            dbconn.Close();
        }
        #endregion Methods & functions        
    }
}