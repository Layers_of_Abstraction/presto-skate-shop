﻿using PrestoSkateShop.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace PrestoSkateShop.TableClasses
{
    public class SaleLine
    {
        #region Object & Query Declarations

        private DataSet _ds = new DataSet();
        private DataRow _dr = null;
        int _intMaxRows;
        int _intCounter = 0;

        DatabaseConnection _objConnect = new DatabaseConnection(Models.Strings.CONNECTION_STRING);

        //Fields declarations
        private const string TABLE_NAME = "tblSaleLines";
        private const string PRIMARY_KEY = "SaleLineID";
        private const string FOREIGN_KEY_PRODUCT = "ProductID";
        private const string FOREIGN_KEY_SALE = "SaleID";
        private const string FIELD_LINE_PRICE = "LinePrice";
        private const string FIELD_QUANTITY = "LineQuantity";

        //Query declarations
        private const string QUERY_SELECT = "SELECT * FROM " + TABLE_NAME;
        private const string QUERY_SELECT_ONE = "SELECT * FROM " + TABLE_NAME + " WHERE " + PRIMARY_KEY + "=({0})";

        private const string QUERY_UPDATE = "UPDATE " + TABLE_NAME +    //CONTINUED...
                                           " SET ProductID =({1}), SaleID=({2}), LinePrice=({3}), LineQuantity =({4}), WHERE " + PRIMARY_KEY + "=({0})";

        private const string QUERY_INSERT = "INSERT INTO " + TABLE_NAME +  //CONTINUED... 
                                            " ( ProductID, SaleID, LinePrice, LineQuantity" +//CONTINUED...
                                            ") VALUES ('{0}', '{1}', {2}, {3})";

        private const string QUERY_DELETE = "DELETE FROM " + TABLE_NAME + " WHERE " + PRIMARY_KEY + "= ({0})";

        #endregion Object & Query Declarations

        #region Properties

        //ID is PK
        public long ID { get; set; }        
        
        //ProductID & SaleID are both FKs 
        public long ProductID { get; set; }
        public long SaleID { get; set; }
        public decimal LinePrice { get; set; }
        public long LineQuantity { get; set; }

        #endregion

        #region Initalizers & Constructors

        public SaleLine() { }

        /// <summary>
        /// Displays a whole grid of data
        /// </summary>
        /// <param name="ds">The datagrid that is being filled with data and
        /// passed back to the presentation layer once it's filled</param>
        /// <returns>data set after it's filled</returns>
        public DataSet loadData(DataSet ds)
        {
            _objConnect.ConnString = Models.Strings.CONNECTION_STRING;
            //SELECT all from SaleLines
            _objConnect.QueryString = QUERY_SELECT;
            //GET table from data set in connection class.
            ds = _objConnect.getDataset;
            //PUT all data into class data set.
            _intMaxRows = ds.Tables[0].Rows.Count;
            //Count out or the data rows in data set.
            _dr = ds.Tables[0].Rows[_intCounter];
            return ds;
        }

        /// <summary>
        /// Loads currently selected record 
        /// </summary>
        /// <param name="id">The Primary key to identify the current record</param>
        /// <returns>The current record</returns>
        public static SaleLine getRecordByID(long id)
        {
            string query = string.Format(QUERY_SELECT_ONE, id);
            OleDbConnection dbConn = new OleDbConnection(Models.Strings.CONNECTION_STRING);
            OleDbCommand cmd = new OleDbCommand(query, dbConn);
            SaleLine result = new SaleLine();

            dbConn.Open();

            using (OleDbDataReader reader = cmd.ExecuteReader())
            {
                reader.Read();
                result.ID = long.Parse(reader[0].ToString());
                result.ProductID = long.Parse(reader[1].ToString());
                result.SaleID = long.Parse(reader[2].ToString());                
                result.LinePrice = decimal.Parse(reader[3].ToString());
                result.LineQuantity = long.Parse(reader[4].ToString());
            }
            dbConn.Close();
            return result;
        }

        #endregion Initalizers & Constructors

        #region Methods & functions

        /// <summary>
        /// Binds the data to a user event based on wether the record
        /// is blank. If it's an existing record then the record
        /// get's updated. It's all determinded. 
        /// </summary>
        public void SaveData()
        {

            if (ID == 0)
            {
                AddNewRecord();
            }

            else
            {
                UpdateRecord();
            }
            _objConnect.updateDB(_ds);
        }

        /// <summary>
        /// This method is bound to the SaveData() method and will add a record from the user event that calls 
        /// the SaveData method.  
        /// </summary>
        private void AddNewRecord()
        {
            string query = "";
            query = string.Format(QUERY_INSERT, this.ProductID, this.SaleID, this.LinePrice, this.LineQuantity);
            OleDbConnection dbConn = new OleDbConnection(Models.Strings.CONNECTION_STRING);
            OleDbCommand dbCmd = new OleDbCommand(query, dbConn);
            dbConn.Open();
            dbCmd.ExecuteNonQuery();
            dbConn.Close();
        }

        /// <summary>
        /// This method is bound to the SaveData() method and will 
        /// update a record from the user event that calls 
        /// the SaveData method.  
        /// </summary>
        private void UpdateRecord()
        {
            string query = "";
            query = string.Format(QUERY_UPDATE, this.ID, this.ProductID, this.SaleID, this.LinePrice, this.LineQuantity);
            OleDbConnection dbConn = new OleDbConnection(Models.Strings.CONNECTION_STRING);
            OleDbCommand dbCmd = new OleDbCommand(query, dbConn);
            dbConn.Open();
            dbCmd.ExecuteNonQuery();
            dbConn.Close();
        }

        /// <summary>
        /// This method will simply delete the current record selected by a user event. 
        /// It is independent of other methods and can be called all by it's self. 
        /// </summary>
        public void DeleteRecord()
        {
            string query = "";
            query = string.Format(QUERY_DELETE, this.ID);
            OleDbConnection dbconn = new OleDbConnection(Models.Strings.CONNECTION_STRING);
            OleDbCommand cmd = new OleDbCommand(query, dbconn);
            dbconn.Open();
            cmd.ExecuteNonQuery();
            dbconn.Close();
        }
        #endregion Methods & functions
    }
}