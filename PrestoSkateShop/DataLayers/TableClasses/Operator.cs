﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Diagnostics;
using System.Data;
using PrestoSkateShop.Models;

namespace PrestoSkateShop.TableClasses
{
    public class Operator
    {        
        #region Object & Query Declarations

        private DataSet _ds = new DataSet();
        private DataRow _dr = null;
        int _intMaxRows;
        int _intCounter = 0;

        DatabaseConnection _objConnect = new DatabaseConnection(Models.Strings.CONNECTION_STRING);

        //Fields declarations
        private const string TABLE_NAME = "tblOperators";
        private const string PRIMARY_KEY = "OperatorID";
        private const string FIELD_NAME = "OperatorName";
        private const string FIELD_PIN = "OperatorPin";
        private const string FIELD_SALES = "OperatorSales";
        private const string FIELD_ADMIN = "OperatorAdmin";
        private const string FILED_CUSTOMER = "OperatorCustomer";        
        private const string FIELD_F_NAME = "OperatorFName";
        private const string FIELD_L_NAME = "OperatorLName";


        //Query declarations
        private const string QUERY_SELECT = "SELECT * FROM " + TABLE_NAME;

        private const string QUERY_SELECT_ONE = "SELECT * FROM " + TABLE_NAME + " WHERE " + PRIMARY_KEY + " =({0})";

        private const string QUERY_UPDATE = "UPDATE " + TABLE_NAME + " SET  OperatorName =('{1}'), OperatorPin=('{2}'), OperatorSales =({3})," +
                                            "OperatorAdmin =({4}), OperatorCustomer =({5}), OperatorFName=('{6}'), OperatorLName =('{7}') " +
                                            "WHERE " + PRIMARY_KEY + " =({0})";

        private const string QUERY_INSERT = "INSERT INTO " + TABLE_NAME + " ( OperatorName , OperatorPin, OperatorSales, OperatorAdmin," +
                                            "OperatorCustomer, OperatorFName, OperatorLName) VALUES" + 
                                            "('{0}', '{1}', {2}, {3}, {4}, '{5}', '{6}', '{7}')";

        private const string QUERY_DELETE = "DELETE FROM " + TABLE_NAME + " WHERE " + PRIMARY_KEY + " = ({0})";

        #endregion Object & Query Declarations

        #region Properties

        //ID is PK
        public long ID { get; set; }
        public string OperatorName { get; set; }
        public string OperatorPin { get; set; }
        public bool Sales { get; set; }
        public bool Admin { get; set; }
        public bool Customer { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        #endregion        

        #region Initalizers & Constructors

        public Operator() { }      

        /// <summary>
        /// displays a whole grid of data
        /// </summary>
        /// <param name="ds">The datagrid that is being filled with data and
        /// passed back to the presentation layer once it's filled</param>
        /// <returns>data set after it's filled</returns>
        public DataSet loadData(DataSet ds)
        {
            _objConnect.ConnString = Models.Strings.CONNECTION_STRING;
            //SELECT all from products
            _objConnect.QueryString = QUERY_SELECT;
            //GET table from data set in connection class.
            ds = _objConnect.getDataset;
            //PUT all data into class data set.
            _intMaxRows = ds.Tables[0].Rows.Count;
            //Count out or the data rows in data set.
            _dr = ds.Tables[0].Rows[_intCounter];
            return ds;
        }

        /// <summary>
        /// Loads currently selected record 
        /// </summary>
        /// <param name="id">The Primary key to identify the current record</param>
        /// <returns>The current record</returns>
        public static Operator getRecordByID(long id)
        {
            string query = string.Format(QUERY_SELECT_ONE, id);
            OleDbConnection dbConn = new OleDbConnection(Models.Strings.CONNECTION_STRING);
            OleDbCommand cmd = new OleDbCommand(query, dbConn);
            Operator result = new Operator();

            dbConn.Open();

            using (OleDbDataReader reader = cmd.ExecuteReader())
            {
                reader.Read();
                result.ID = long.Parse(reader[0].ToString());
                result.OperatorName = reader[1].ToString();
                result.OperatorPin  = reader[2].ToString();
                result.Sales = bool.Parse(reader[3].ToString());
                result.Admin = bool.Parse(reader[4].ToString());
                result.Customer = bool.Parse(reader[5].ToString());
                result.FirstName = reader[6].ToString();
                result.LastName = reader[7].ToString();
            }
            dbConn.Close();
            return result;
        }

        #endregion Initalizers & Constructors

        #region Methods & functions

        /// <summary>
        /// Binds the data to a user event based on wether the record
        /// is blank. If it's an existing record then the record
        /// get's updated. 
        /// </summary>
        public void SaveData()
        {
            
            if (ID == 0)
            {
                AddNewRecord();
            }

            else
            {
                UpdateRecord();
            }            
            _objConnect.updateDB(_ds);            
        }

        /// <summary>
        /// This method is bound to the SaveData() method and will
        /// add a record from the user event that calls 
        /// the SaveData method.  
        /// </summary>
        private void AddNewRecord()
        {
            string query = "";
            query = string.Format(QUERY_INSERT, this.OperatorName, this.OperatorPin, this.Sales, this.Admin, this.Customer, this.FirstName, this.LastName);
            OleDbConnection dbConn = new OleDbConnection(Models.Strings.CONNECTION_STRING);
            OleDbCommand dbCmd = new OleDbCommand(query, dbConn);
            dbConn.Open();
            dbCmd.ExecuteNonQuery();            
            dbConn.Close();
        }

        /// <summary>
        /// This method is bound to the SaveData() method and will 
        /// update a record from the user event that calls 
        /// the SaveData method.  
        /// </summary>
        private void UpdateRecord()
        {
            string query = "";
            query = string.Format(QUERY_UPDATE, this.ID, this.OperatorName, this.OperatorPin, this.Sales, this.Admin, this.Customer, this.FirstName, this.LastName);
            OleDbConnection dbConn = new OleDbConnection(Models.Strings.CONNECTION_STRING);
            OleDbCommand dbCmd = new OleDbCommand(query, dbConn);
            dbConn.Open();
            dbCmd.ExecuteNonQuery();
            dbConn.Close();
        }

        /// <summary>
        /// This method will simply delete the current record selected by a user event. It is independent of other methods and
        /// can be called all by it's self. 
        /// </summary>
        public void DeleteRecord()
        {
            string query = "";
            query = string.Format(QUERY_DELETE, this.ID);
            OleDbConnection dbconn = new OleDbConnection(Models.Strings.CONNECTION_STRING);
            OleDbCommand cmd = new OleDbCommand(query, dbconn);
            dbconn.Open();
            cmd.ExecuteNonQuery();
            dbconn.Close();
        }

        #endregion Methods & functions
    }
}