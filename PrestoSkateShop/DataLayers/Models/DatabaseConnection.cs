﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;

namespace PrestoSkateShop.Models
{
    /// <summary>
    /// Connects SQL Database, querys and processes external 
    /// methods and functions SELECT,INSERT,UPDATE and DELETE.
    /// </summary>
    public class DatabaseConnection
    {
        #region Object Declarations 
        private string _strConn;
        private string _strQuery;        
        #endregion Object Declarations 
        
        #region Constructors

        /// <summary>
        /// My Default constructor
        /// </summary>
        public DatabaseConnection() { }

        /// <summary>
        /// Constructor overload that returns the 
        /// connection string to wherever you call it.
        /// </summary>
        /// <param name="strConnect">The connection string in this case
        /// will get sent back to our a function call outside this class. </param>
        public DatabaseConnection(string strConnect) 
        {
            _strConn = strConnect;
        }
        
        #endregion Constructor

        #region Properties
        public string QueryString
        {
            set { _strQuery = (value); }
        }
        
        public string ConnString
        {
            set { _strConn = (value); }
        }        

        public DataSet getDataset
        {
            get { return CreateDataSet(); }
        }        
        #endregion Properties

        #region Methods & Functions

        /// <summary>
        /// Opens and closes database and logs any errors to 
        /// text file upon compiling.
        /// </summary>
        /// <returns>database table to user</returns>
        private DataSet CreateDataSet()
        {
            SqlConnection conn = new SqlConnection(_strConn);
            SqlDataAdapter da = new SqlDataAdapter(_strQuery, _strConn);
            DataSet ds = new DataSet(); 

            try
            {
                conn.Open();
                da.Fill(ds, "Table_Data_1");                                
            }

            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                FileLogger fw = new FileLogger("ErrorLog", "Text");
                fw.write(ex.ToString());
            }

            finally
            {
                conn.Close();
            }
            return ds;
        }

        /// <summary>
        /// - Processes external queries and allows them to change data in Database.
        /// - Logs any errors to text file if query is illogical to C# compiler.
        /// </summary>
        /// <param name="ds">Current table is written into this data set and altered. </param>
        public void updateDB(DataSet ds)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(_strQuery, _strConn);
                SqlCommandBuilder cb = new SqlCommandBuilder(da);
                cb.DataAdapter.Update(ds.Tables[0]);
            }

            catch (Exception ex)
            {          
                Debug.WriteLine("ERROR in updateDB(): " + ex.Message);
                FileLogger fw = new FileLogger("ErrorLog", "Text");
                fw.write(ex.ToString());
            }
        }
        #endregion Methods & Functions
    }
}
