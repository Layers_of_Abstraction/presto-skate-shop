﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrestoSkateShop.Models
{
    /// <summary>
    /// - Stores string refrences for all web forms, Primary Keys and Foreign Keys.
    /// - Centeralizes all declarative info so it's assessible across the 
    ///   whole Solution.
    /// </summary>
    public static class Strings
    {
        //(LocalDB)\MSSQLLocalDB
        public const string CONNECTION_STRING = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|PrestoSkateShop.mdf;Integrated Security=True";
        public static class Pages
        {
            public const string Default = "Default";
           
            public const string ProductList = "ProductList";
            public const string ProductDetails = "ProductDetails";

            public const string OperatorList = "OperatorList";
            public const string OperatorDetails = "OperatorDetails";
            public const string Orders = "Orders";
        }

        public static class SessionVariables
        {
            public const string ProductID = "ProductID";//pk
            public const string OperatorID = "OperatorID";//pk
            public const string SaleID = "SaleID";
        }
    }
}