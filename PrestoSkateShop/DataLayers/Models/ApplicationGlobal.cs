﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Diagnostics;

namespace PrestoSkateShop.Models
{
    /// <summary>
    /// Class designed for global
    /// </summary>
    public static class ApplicationGlobal
    {
        public static string CONNECTION_STRING = Models.Strings.CONNECTION_STRING;

        public static class LoggedInOperator
        {
            public static int UserID = 0;
            public static string Username = null;
            public static string Password = null;
            public static List<string> Roles = new List<string>();

            /// <summary>
            /// Checks to see if the user is logged and credentials
            /// match up when compared to the OperatorID
            /// </summary>
            public static bool LoggedIn 
            { 
                get
                {
                    if (UserID != 0 && !string.IsNullOrEmpty(Username))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            public static bool HasRole(string role)
            {
                if (Roles.Contains(role))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        
            public static bool LogIn(string _uname, string _pwd)
            {
                try
                {
                    OleDbConnection dbConn = new OleDbConnection(CONNECTION_STRING);
                    OleDbCommand dbCmd = new OleDbCommand("SELECT * FROM qryOperatorLogin", dbConn);
                    dbCmd.Parameters.Add(new OleDbParameter("UserLogin", _uname));
                    dbCmd.Parameters.Add(new OleDbParameter("UserPassword", _pwd));
                    dbConn.Open();
                    var reader = dbCmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            UserID = (int)reader["OperatorID"];
                            Username = (string)reader["OperatorName"];

                            object x = reader["OperatorSales"];
                            bool operatorSales = Boolean.Parse(reader["OperatorSales"].ToString());
                            
                            if (operatorSales) 
                            {
                                Roles.Add("OperatorSales");
                            }


                            bool operatorAdmin = Boolean.Parse(reader["OperatorAdmin"].ToString());
                            
                            if (operatorAdmin)
                            {
                                Roles.Add("OperatorAdmin");                            
                            }

                        }

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ERROR: " + ex.Message);
                    return false;                    
                }
            }
        }

        public static Dictionary<int, string> Roles = new Dictionary<int, string>();

        public static void InitData()
        {
            OleDbConnection dbConn = new OleDbConnection(CONNECTION_STRING);
            OleDbCommand dbCmd = new OleDbCommand("SELECT * FROM tblRoles", dbConn);
            dbConn.Open();
            var reader = dbCmd.ExecuteReader();
            while(reader.Read())
            {
                Roles.Add((int)reader["RoleID"], (string)reader["RoleTitle"]);
            }
            dbConn.Close();
        }
    }
}