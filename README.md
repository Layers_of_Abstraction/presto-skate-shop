A data storage app made in ASP.NET and code behind C# with a local backend SQL Server database. For a made up company called Presto Skate Shop. It uses the NuGet Library called FastMember. 

Project is for data Presto need to keep track of including data from the following.  

  •	Products 
  
  •	Sales 
  
  •	Operators 
  
  •	Sale History 
  
  •	Customers 
  
  •	Product Categories 

Web program is to have a login system integrated for users to store, delete and manipulate certain data depending on their login. The users include:  

  •	Administrator. Has access to database operators, products and categories. 
  
  •	Sales Person. Has access to Sales and Read only access to products. 
  
  •	Customer. Has a basic way to Read-Only Products and make a sale record. Also has basic way of creating their own acount.  
  
  Requirements:  
    
  •.NET 4.5 
  
  • Visual Studio 2013 
  
  • SQL Server 2014   
  
  Please help make contributes if you are interested.


UPDATES:

17/1/1

Static lists for products and categories can now be bindied now to the ProductList datagridview.  
